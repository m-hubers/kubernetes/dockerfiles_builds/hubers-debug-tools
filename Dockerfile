FROM amazonlinux:2

ARG USERNAME=ec2-user
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN yum update -y \
  && yum groupinstall "AWS Tools" -y \
  && yum install -y sudo git curl zsh less jq groff bind-utils mtr iperf3 lsof nmap-ncat iproute wget \
  && amazon-linux-extras install python3.8 docker postgresql12 nginx1 -y \
  && adduser $USERNAME --user-group --create-home --uid $USER_UID \
  && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
  && chmod 0440 /etc/sudoers.d/$USERNAME \
  && yum clean all
  
USER $USERNAME

RUN sh -c "$(wget -O- https://github.com/deluan/zsh-in-docker/releases/download/v1.1.2/zsh-in-docker.sh)" -- \
    -t https://github.com/denysdovhan/spaceship-prompt \
    -p https://github.com/zsh-users/zsh-autosuggestions \
    -p https://github.com/zsh-users/zsh-completions \
    -p https://github.com/zsh-users/zsh-history-substring-search \
    -p https://github.com/zsh-users/zsh-syntax-highlighting \
    -p 'history-substring-search' \
    -a 'bindkey "\$terminfo[kcuu1]" history-substring-search-up' \
    -a 'bindkey "\$terminfo[kcud1]" history-substring-search-down'

ENTRYPOINT [ "/bin/zsh" ]
CMD ["-l"]
